<?php 

/**
 * Template Name: Sitemap
 *
 * A custom page template without Primary and Secondary sidebars.
 *
 * The "Template Name:" bit above allows this to be selectable
 * from a dropdown menu on the edit page screen.
 *
 * @package WordPress
 * @subpackage Provek
 * @since Provek 1.0
 */

get_header(); ?>

	<!-- BREADCRUMB -->
	<?php get_template_part('template-parts/breadcrumb/content'); ?>

	<!-- BODY CONTENT -->
	<div class="section section--triangles">
		<!-- TRIANGLES -->
		<div class="triangles top-right xsml secondary zindex5"></div>
		<div class="triangles base-left sml primary zindex3"></div>
		<div class="triangles base-right lrg gray zindex1"></div>

		<div class="container">
			<!-- PAGE TITLE -->
			<div class="row pt-md">
				<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
					<h1 class="heading-h3 color-secondary">Sitemap</h1>
				</div>
			</div>

			<!-- BODY -->
			<div class="row pb-sm">
				<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
					<?php if(is_active_sidebar('sitemap')) { ?>
						<?php dynamic_sidebar('sitemap'); ?>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>

	<!-- CLIENTS -->
	<?php get_template_part('template-parts/carousels/content', 'client'); ?>

	<!-- THE PROVEK WAY -->
	<?php get_template_part('template-parts/provek-way/content'); ?>

<?php get_footer(); ?>