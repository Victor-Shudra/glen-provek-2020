<?php 

/**
 * Template Name: Post Listing
 *
 * A custom page template without Primary and Secondary sidebars.
 *
 * The "Template Name:" bit above allows this to be selectable
 * from a dropdown menu on the edit page screen.
 *
 * @package WordPress
 * @subpackage Provek
 * @since Provek 1.0
 */

get_header(); ?>

	<!-- BREADCRUMB -->
	<?php get_template_part('template-parts/breadcrumb/content'); ?>

	<!-- BODY CONTENT -->
	<div class="section section--triangles">
		<!-- TRIANGLES -->
		<div class="triangles top-right xsml secondary zindex5"></div>
		<div class="triangles base-left sml primary zindex3"></div>
		<div class="triangles base-right lrg gray zindex1"></div>

		<!-- PAGE TITLE -->
		<div class="container">
			<div class="row pt-md pb-sm">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<?php $post = get_post(5); ?>
					<h1 class="heading-h3 color-secondary"><?php echo get_the_title($post) ?></h1>
				</div>
			</div>

			<!-- NEWS LISTING -->
			<div class="row pb-sm">
				<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
					<div class="row row-eq-height">
						<?php $args = array(
				            'posts_per_page'    => -1,
				            'post_type'         => 'post',
				            'post_status'       => 'publish',
				        );

				        // SEARCH CATEGORRY
				        if ( $_GET['cat'] ) {
				            $args['tax_query'] = array(
				                array(
				                    'taxonomy' => 'category',
				                    'field'    => 'term_id',
				                    'terms'    => $_GET['cat'],
				                ),
				            );
				        }

				        // SEARCH KEYWORD
				        if ( $_GET['search'] ) {
				            $args['s'] = $_GET['search'];
				        }

	        			$the_query = new WP_Query( $args );

				        if ( $the_query->have_posts() ) :
				            while ( $the_query->have_posts() ) : $the_query->the_post();
				            	get_template_part('template-parts/post/content', get_post_format());
				            endwhile;
				        else : ?>
				        	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				        		<h3 class="heading-h4 color-secondary">Sorry, no results are available</h3>
								<p>Please <a href="/news/" class="link-theme color-primary">click here</a> to reset the filter.</p>
							</div>
				        <?php endif; ?>

				        <?php wp_reset_postdata(); ?>
					</div>
					<?php if (have_posts()) { ?>
						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<?php
									the_posts_pagination(array(
										'mid_size'  => 2,
										'prev_text' => __('&laquo;', 'textdomain'),
										'next_text' => __('&raquo;', 'textdomain'),
									));
								?>
							</div>
						</div>
					<?php } ?>
				</div>

				<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
					<h3 class="heading-h6 color-secondary heading-keyline">Newsletter Signup</h3>
					<?php if (!empty($newsletter)) { ?>
						<p><?php echo $newsletter; ?></p>
					<?php } ?>
					<!-- Begin MailChimp Signup Form -->
					<link href="//cdn-images.mailchimp.com/embedcode/classic-10_7.css" rel="stylesheet" type="text/css">
					<div class="form-block form-block--newsletter">
						<form action="https://provek.us17.list-manage.com/subscribe/post?u=38a4b9030a46e1a275f73017d&amp;id=d0eb1ad44e" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
							<div class="mc-field-group form-block--newsletter-group">
								<label for="mce-EMAIL"><input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL" placeholder="Email Address*"></label>
							</div>
							<div class="mc-field-group form-block--newsletter-group">
								<label for="mce-FNAME"><input type="text" value="" name="FNAME" class="" id="mce-FNAME" placeholder="Full Name"></label>
							</div>
							<div id="mce-responses" class="clear">
								<div class="response" id="mce-error-response" style="display:none"></div>
								<div class="response" id="mce-success-response" style="display:none"></div>
							</div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
							<div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_38a4b9030a46e1a275f73017d_d0eb1ad44e" tabindex="-1" value=""></div>
							<div class="clear"><input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
						</form>
					</div>
					<script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script>
					<noscript>This website requires Javascript. Please enable this to view the site at its best.</noscript>
					<script type='text/javascript'>(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';fnames[1]='FNAME';ftypes[1]='text';}(jQuery));var $mcj = jQuery.noConflict(true);</script>
					<noscript>This website requires Javascript. Please enable this to view the site at its best.</noscript>
					<!--End mc_embed_signup-->
					

					<h3 class="heading-h6 color-secondary heading-keyline">Looking for something specific?</h3>

					<!-- FILTER  -->
					<!-- You searched within:
					<?php
				        //if ( $_GET['cat'] ) {
				            //$cat = get_term( $_GET['cat'], 'category' );
				            //echo $cat->name;
				        //} else {
				        	//echo "All categories";
				        //}
				    ?> -->

				    <div class="form-block form-block--searchandfilter">
						<?php $categories = get_terms( 'category'); ?>
						<form method="get">
							<select name="cat">
								<option value="">All categories</option>
							<?php foreach ( $categories as $category ) : ?>
								<option value="<?php echo $category->term_id; ?>" <?php if ( $_GET['cat'] && $_GET['cat'] == $category->term_id ) echo 'selected'; ?>>
									<?php echo $category->name; ?>
								</option>
							<?php endforeach; ?>
							</select>

							<label for="txtSearch">
								<input id="txtSearch" type="search" name="search" value="<?php echo $_GET['search']; ?>" placeholder="Search Keywords..." />
							</label>

							<button type="submit" class="btn btn-default btn-theme btn-theme--primary">Apply Filter</button>
						</form>
					</div>

					<h3 class="heading-h6 color-secondary heading-keyline">Archive</h3>

					<?php  
						global $wpdb;
						$limit = 0;
						$year_prev = null;
						$months = $wpdb->get_results("SELECT DISTINCT MONTH( post_date ) AS month ,  YEAR( post_date ) AS year, COUNT( id ) as post_count FROM $wpdb->posts WHERE post_status = 'publish' and post_date <= now( ) and post_type = 'post' GROUP BY month , year ORDER BY post_date DESC");
					?>

					<!--	We have a flat list of months and years sorted to group by year. We want to nest the months within the year   -->
					<!--	Open our outer list container -->
					<ul class="archive">
						<!--	Loop through all the months/years	-->
						<?php foreach($months as $month) : $year_current = $month->year; ?>
							<!--	Check if the previous year is not equal to the current year - this indicates the start / end of a year group   -->
							<?php if ($year_current != $year_prev): ?>
								<!--	Check if the previous year is null - this is a special case in that it's the start of the loop. If it's null we don't need to close the previous year item and container tags   -->
								<?php if ($year_prev!==null): ?>
									</li>
								</ul>
								<?php endif; ?>
								<!--	Output the year list item and open the month list container  -->
								<li class="archive-year"><!-- <a href="<?php //bloginfo('url') ?>/<?php //echo $month->year; ?>/"> --><a href="#"><?php echo $month->year; ?> Archive</a><!-- </a> -->
									<ul class="archive-month">
	    					<?php else: ?>
	    					<!--	Output the month list item	-->
						    <li><a href="<?php bloginfo('url') ?>/<?php echo $month->year; ?>/<?php echo date("m", mktime(0, 0, 0, $month->month, 1, $month->year)) ?>"><?php echo date_i18n("F Y", mktime(0, 0, 0, $month->month, 1, $month->year)) ?></a></li>
							<?php endif; ?>
						<!--	Set the previous year to the current year for use in the next loop run	-->
						<?php $year_prev = $year_current; ?>
						<?php endforeach; ?>
						<!--	Close the last year list container and our outer list container -->
						</ul>
					</ul>

				</div>
			</div>
		</div>
	</div>

	<!-- THE PROVEK WAY -->
	<?php get_template_part('template-parts/provek-way/content'); ?>

<?php get_footer(); ?>