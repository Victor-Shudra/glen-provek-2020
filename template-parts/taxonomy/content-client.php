<?php
/**
* Template part for displaying Shop Archive
*
* @link https://codex.wordpress.org/Template_Hierarchy
*
* @package WordPress
* @subpackage Provek
* @since 1.0
* @version 1.0
*/

?>
<!-- CONTENT -->
<?php 
	$post = get_post(801);

	$leftcolumn = get_field('grp_content_left', $post->ID);
	$rightcolumn = get_field('grp_content_right', $post->ID);
	$calltoaction = get_field('grp_content_calltoaction', $post->ID);
?>

<div class="section section--triangles">
	<!-- TRIANGLES -->
	<div class="triangles top-right xsml secondary zindex5"></div>
	<?php if (!empty($rightcolumn['fld_content_right_tri_image'])): ?>
	<div class="top-right--img">
		<div  class="top-right--img--bkd" style="background:url(<?php echo $rightcolumn['fld_content_right_tri_image']['url']; ?>) 0 0 no-repeat;background-size: 100%;"></div>
	</div>
	<?php endif; ?>
	<div class="triangles base-left sml primary zindex3"></div>
	<div class="triangles base-right lrg gray zindex1"></div>

	<div class="container">
		<!-- PAGE TITLE -->
		<div class="row pt-md pb-sm">
			<?php if (!empty($rightcolumn['fld_content_right_image'])): ?>
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
			<?php else: ?>
				<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
			<?php endif; ?>
					<h1 class="heading-h3 color-secondary"><?php the_title() ?></h1>
				</div>
			<?php if (!empty($rightcolumn['fld_content_right_image'])): ?>
				<div class="col-xs-4 col-sm-2 col-sm-offset-4 col-md-2 col-md-offset-4 col-lg-2 col-lg-offset-4">
					<div class="thumbnail-theme thumbnail-theme--image"><img src="<?php echo $rightcolumn['fld_content_right_image']['url']; ?>" alt="<?php echo $rightcolumn['fld_content_right_image']['alt']; ?>" class="img-responsive" /></div>
				</div>
			<?php endif; ?>
		</div>

		<!-- BODY -->
		<?php if (!empty($leftcolumn['fld_content_left_excerpt'])): ?>
			<div class="row pb-sm">
				<?php if (!empty($rightcolumn['fld_content_right_body'])): ?>
					<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
				<?php else: ?>		
					<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
				<?php endif; ?>
					<?php if (!empty($leftcolumn['fld_content_left_title'])): ?>
						<h3 class="heading-h6 color-secondary heading-keyline"><?php echo $leftcolumn['fld_content_left_title']; ?></h3>	
					<?php endif; ?>
					<h3 class="heading-h4 color-secondary"><?php echo $leftcolumn['fld_content_left_excerpt']; ?></h3>
					<?php if (!empty($leftcolumn['fld_content_left_body'])): ?>
						<div class="content"><?php echo $leftcolumn['fld_content_left_body']; ?></div>
					<?php endif; ?>
					<?php if (isset($leftcolumn['fld_content_left_linktopage']['url'])): ?>
						<?php if (!empty($leftcolumn['fld_content_left_linktopage']['target'])): ?>
							<a class="btn btn-default btn-theme btn-theme--primary" href="<?php echo $leftcolumn['fld_content_left_linktopage']['url']; ?>" target="<?php echo $leftcolumn['fld_content_left_linktopage']['target']; ?>"><?php echo $leftcolumn['fld_content_left_linktopage']['title']; ?></a>
						<?php else: ?>
							<a class="btn btn-default btn-theme btn-theme--primary" href="<?php echo $leftcolumn['fld_content_left_linktopage']['url']; ?>"><?php echo $leftcolumn['fld_content_left_linktopage']['title']; ?></a>
						<?php endif; ?>
					<?php endif; ?>
				</div>
				<?php if (!empty($rightcolumn['fld_content_right_body'])): ?>
					<div class="col-xs-12 col-sm-12 col-md-5 col-md-offset-1 col-lg-5 col-lg-offset-1">
						<?php if (!empty($rightcolumn['fld_content_right_title'])): ?>
							<h3 class="heading-h6 color-secondary heading-keyline"><?php echo $rightcolumn['fld_content_right_title']; ?></h3>
						<?php endif; ?>
						<div class="content"><?php echo $rightcolumn['fld_content_right_body']; ?></div>
						<!-- CALL TO ACTION -->
						<?php if ($calltoaction['fld_calltoaction_type'] == 'page_link'): ?>
							<?php if (isset($calltoaction['fld_calltoaction_link']['url'])): ?>
								<?php if (!empty($calltoaction['fld_calltoaction_link']['target'])): ?>
									<a class="btn btn-default btn-theme btn-theme--primary" href="<?php echo $calltoaction['fld_calltoaction_link']['url']; ?>" target="<?php echo $calltoaction['fld_calltoaction_link']['target']; ?>"><?php echo $calltoaction['fld_calltoaction_link']['title']; ?></a>
								<?php else: ?>
									<a class="btn btn-default btn-theme btn-theme--primary" href="<?php echo $calltoaction['fld_calltoaction_link']['url']; ?>"><?php echo $calltoaction['fld_calltoaction_link']['title']; ?></a>
								<?php endif; ?>
							<?php endif; ?>
						<?php elseif ($calltoaction['fld_calltoaction_type'] == 'contact'): ?>
							<div class="calltoaction">
								<div class="triangles base-right med black opacity1 zindex1"></div>
								<div class="row">
									<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
										<p class="calltoaction__title"><?php echo $calltoaction['grp_calltoaction_contact']['fld_contact_title']; ?></p>
										<p class="calltoaction__body"><?php echo $calltoaction['grp_calltoaction_contact']['fld_contact_excerpt']; ?></p>
									</div>
									<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
										<?php if (isset($calltoaction['grp_calltoaction_contact']['fld_contact_link']['url'])): ?>
											<?php if (!empty($calltoaction['grp_calltoaction_contact']['fld_contact_link']['target'])): ?>
												<a class="btn btn-default btn-theme btn-theme--white calltoaction__btn" href="<?php echo $calltoaction['grp_calltoaction_contact']['fld_contact_link']['url']; ?>" target="<?php echo $calltoaction['grp_calltoaction_contact']['fld_contact_link']['target']; ?>"><?php echo $calltoaction['grp_calltoaction_contact']['fld_contact_link']['title']; ?></a>
											<?php else: ?>
												<a class="btn btn-default btn-theme btn-theme--white calltoaction__btn" href="<?php echo $calltoaction['grp_calltoaction_contact']['fld_contact_link']['url']; ?>"><?php echo $calltoaction['grp_calltoaction_contact']['fld_contact_link']['title']; ?></a>
											<?php endif; ?>
										<?php endif; ?>
									</div>
								</div>
							</div>
						<?php elseif ($calltoaction['fld_calltoaction_type'] == 'application'): ?>
							<div class="calltoaction">
								<div class="triangles base-right med black opacity1 zindex1"></div>
								<div class="row">
									<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
										<p class="calltoaction__title calltoaction__keyline"><?php echo $calltoaction['grp_calltoaction_application']['fld_application_title']; ?></p>
									</div>
								</div>
								<div class="row">
									<div class="col-xs-7 col-sm-7 col-md-7 col-lg-7">
										<p class="calltoaction__email">Download a form and send to:<br><a href="mailto:<?php echo $calltoaction['grp_calltoaction_application']['fld_application_email']; ?>"><?php echo $calltoaction['grp_calltoaction_application']['fld_application_email']; ?></a></p>
									</div>
									<div class="col-xs-5 col-sm-5 col-md-5 col-lg-5">
										<?php if (isset($calltoaction['grp_calltoaction_application']['fld_application_link']['url'])): ?>
											<?php if (!empty($calltoaction['grp_calltoaction_application']['fld_application_link']['target'])): ?>
												<a class="btn btn-default btn-theme btn-theme--white calltoaction__download-btn" href="<?php echo $calltoaction['grp_calltoaction_application']['fld_application_link']['url']; ?>" target="<?php echo $calltoaction['grp_calltoaction_application']['fld_application_link']['target']; ?>"><?php echo $calltoaction['grp_calltoaction_application']['fld_application_link']['title']; ?></a>
											<?php else: ?>
												<a class="btn btn-default btn-theme btn-theme--white calltoaction__download-btn" href="<?php echo $calltoaction['grp_calltoaction_application']['fld_application_link']['url']; ?>"><?php echo $calltoaction['grp_calltoaction_application']['fld_application_link']['title']; ?></a>
											<?php endif; ?>
										<?php endif; ?>
									</div>
								</div>
								<div class="row">
									<div class="col-xs-7 col-sm-7 col-md-7 col-lg-7">
										<p class="calltoaction__body">Alternatively book direct by calling:</p>
									</div>
									<div class="col-xs-5 col-sm-5 col-md-5 col-lg-5">
										<p class="calltoaction__telephone"><?php echo $calltoaction['grp_calltoaction_application']['fld_application_tel']; ?></p>
									</div>
								</div>
							</div>
						<?php else: ?>
						<?php endif; ?>
					</div>
				<?php endif; ?>
			</div>
		<?php endif; ?>

		<!-- CLIENT LISTING -->
		<div class="row pb-sm">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<h3 class="heading-h6 color-secondary heading-keyline">Our clients</h3>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<div class="row row-eq-height">			
					<?php foreach(get_terms('industry', array('parent' => 0 )) as $parent_term) { ?>
						<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
							<div class="calltoaction calltoaction--client">
								<div class="triangles base-right med black opacity1 zindex1"></div>
								
								<!-- PARENT CATEGORY -->
								<h3 class="calltoaction__title"><?php echo $parent_term->name; ?></h3>

								<!-- SUB CATEGORY -->
								<?php foreach(get_terms('consult', array('hide_empty' => false, 'parent' => $parent_term->term_id)) as $child_term) { ?>
									<h6><?php echo $child_term->name; ?></h6>
								<?php } ?>

								<?php
									$args = array(
										'post_type' => 'clients',
										'tax_query' => array(
											array(
												'taxonomy' => 'industry',
												'field'    => 'term_id',
												'terms'    => $parent_term->term_id,
											),
										),
									);
									$the_query = new WP_Query($args);
								?>

								<?php while ( $the_query->have_posts() ) : $the_query->the_post();
									$cats = get_the_terms($post->ID, 'industry');
									$has_child = false;
									foreach ($cats as $cat) {
										if ($cat->parent==$parent_term->term_id) {
											$has_child = true;
										}
									}
								?>

								<?php if (!$has_child) { 
									$pagelink = get_field('fld_client_linktopage');
								?>
									
									<?php if (!empty($pagelink)) { ?>
										<a class="calltoaction__client" href="<?php echo $pagelink['url']; ?>"><?php echo get_the_title(); ?></a>
									<?php } else { ?>
										<span class="calltoaction__client"><?php echo get_the_title(); ?></span>
									<?php } ?>
								
								<?php } ?>
								
								<?php endwhile; wp_reset_postdata(); ?>
							</div>
						</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
</div>