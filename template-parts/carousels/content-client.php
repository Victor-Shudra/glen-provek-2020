<?php
/**
 * Template part for displaying Client Carousel
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Provek
 * @since 1.0
 * @version 1.0
 */

?>
<div class="section section--triangles section--triangles--auto">
	<div class="triangles base-left med gray zindex1"></div>
	<div class="container">
		<?php $clients = new WP_Query(array('post_type' => 'clients', 'posts_per_page' => -1, 'orderby' => 'rand')); ?>
		<?php if ($clients->have_posts()) : ?>
			<div class="row pt-md">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<h3 class="heading-h6 color-secondary heading-keyline">Who we've helped</h3>
				</div>
			</div>

			<div class="row pb-md">
				<div id="clientCarousel" class="carousel carousel-block slide carousel-block--generic" data-ride="carousel">
					<!-- CONTROLS -->
					<a class="btn btn-default carousel-block__btn carousel-block__btn-left color-primary" href="#clientCarousel" data-slide="prev">Previous</a>
					<a class="btn btn-default carousel-block__btn carousel-block__btn-right color-primary" href="#clientCarousel" data-slide="next">Next</a>
					<div class="carousel-inner carousel-block__inner">
              			<?php 
              				$rowCount = $clients->post_count;
							$first = true;
							$pageCounter = 0;
							$rowCounter = 0;
						?>

            			<?php while ($clients->have_posts()) : $clients->the_post(); 
							$pageCounter++;
                			$rowCounter++;
                			$image = get_field('fld_client_image');
                			$pagelink = get_field('fld_client_linktopage');
						?>

						<?php if ($pageCounter==1): ?>
							<div class="item<?php echo $first ? ' active' : ''; ?>">
						<?php endif; ?>
								<div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
									<?php if (!empty($pagelink)) { ?>
										<?php if (!empty($image)) { ?>
											<a href="<?php echo $pagelink['url']; ?>" class="thumbnail-theme thumbnail-theme--logo"><img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" class="img-responsive" /></a>
										<?php } ?>
										<p><a href="<?php echo $pagelink['url']; ?>"><?php echo get_the_title(); ?></a></p>
									<?php } else { ?>
										<div class="thumbnail-theme thumbnail-theme--logo"><img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" class="img-responsive" /></div>
										<p><?php echo get_the_title(); ?></p>
									<?php } ?>
								</div>
						<?php if ($pageCounter==4 || $rowCounter==$rowCount): ?>
							</div>
						<?php endif; ?>


						<?php $first = false; ?>
						<?php if ($pageCounter==4) { $pageCounter = 0;} ?>
						
						<?php endwhile; wp_reset_query(); ?>
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<a class="btn btn-default btn-theme btn-theme--primary" href="/our-clients/">View all clients</a>
				</div>
			</div>
		<?php endif; ?>
	</div>
</div>