<?php
/**
 * Template part for displaying Staff Post
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Provek
 * @since 1.0
 * @version 1.0
 */

?>
<?php 
	$image = get_field('fld_staff_image');
//	$position = get_field('fld_staff_position');
	$position = get_field('fld_staff_tel');
	$telephone = get_field('fld_staff_tel');
	$email = get_field('fld_staff_email');
?>
<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
	<div class="post-link">
		<a class="post-link__link" href="<?php the_permalink(); ?>"></a>
		<?php if (!empty($image)) { ?>
			<div class="thumbnail-theme thumbnail-theme--listing4col"><img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" class="img-responsive" /></div>
		<?php } ?>
		<div class="post-link__inner">
			<h3 class="post-link__title"><?php the_title() ?></h3>
			<?php if (!empty($telephone) || !empty($email)) { ?>
				<p class="post-link__body"><?php echo $telephone; ?><br>
				<a href="mailto:<?php echo $email; ?>"><?php echo $email; ?></a></p>
			<?php } ?>
			<div class="post-link__btn">Read more</div>
		</div>
	</div>
</div>