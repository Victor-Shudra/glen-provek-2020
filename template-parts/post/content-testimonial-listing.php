<?php
/**
 * Template part for displaying Testimonial Post
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Provek
 * @since 1.0
 * @version 1.0
 */

?>
<?php 
	$testimonial = get_field('fld_testimonial_body');
	$client = get_field('fld_testimonial_client');
	$position = get_field('fld_testimonial_client_position');
	$date = get_field('fld_testimonial_date');
?>
<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
	<?php if (!empty($testimonial)) { ?>
		<p class="heading-h7 color-secondary"><?php echo $testimonial; ?></p>
	<?php } ?>
	<?php if (!empty($position)) { ?>
		<p><?php echo $client; ?>, <?php echo $position; ?>, <?php echo $date; ?></p>
	<?php } else { ?>
		<p><?php echo $client; ?>, <?php echo $date; ?></p>
	<?php } ?>
</div>