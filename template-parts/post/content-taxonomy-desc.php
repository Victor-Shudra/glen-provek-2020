<?php
/**
 * Template part for displaying Course Post with Description
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Provek
 * @since 1.0
 * @version 1.0
 */

?>
<?php 
	$leftcolumn = get_field('grp_content_left');
?>
<div class='col-xs-12 col-sm-3 col-md-3 col-lg-3'>
	<div class="category-calltoaction">
		<div class="triangles base-right med black opacity1 zindex1"></div>
		<a class="category-calltoaction__link" href="<?php the_permalink(); ?>"></a>
		<h3 class="category-calltoaction__desctitle"><?php the_title(); ?></h3>
		<?php if (!empty($leftcolumn['fld_content_left_excerpt'])): ?>
			<p class="category-calltoaction__body"><?php echo $leftcolumn['fld_content_left_excerpt']; ?><br><br><strong>Find out more</strong></p>
		<?php endif; ?>
	</div>
</div>