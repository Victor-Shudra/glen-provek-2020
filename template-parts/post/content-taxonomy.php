<?php
/**
 * Template part for displaying Course Post
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Provek
 * @since 1.0
 * @version 1.0
 */

?>
<div class='col-xs-12 col-sm-3 col-md-3 col-lg-3'>
	<div class="category-calltoaction">
	<div class="triangles base-right med black opacity1 zindex1"></div>
		<a class="category-calltoaction__link" href="<?php the_permalink(); ?>"></a>
		<h3 class="category-calltoaction__title"><?php the_title(); ?></h3>
	</div>
</div>