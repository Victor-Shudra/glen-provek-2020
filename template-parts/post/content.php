<?php
/**
 * Template part for displaying Blog Post
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Provek
 * @since 1.0
 * @version 1.0
 */

?>
<?php 
	$leftcolumn = get_field('grp_content_left');
	$rightcolumn = get_field('grp_content_right');
?>
<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
	<div class="post-link">
		<a class="post-link__link" href="<?php the_permalink(); ?>"></a>
		<?php if (!empty($rightcolumn['fld_content_right_image'])): ?>
			<!-- <?php //echo wp_get_attachment_image($rightcolumn['fld_content_right_image']['id'],'list-image size',false,array('alt' => $rightcolumn['fld_content_right_image']['alt'], 'class' => 'img-responsive')); ?> -->
			<div class="thumbnail-theme thumbnail-theme--listing3col"><img src="<?php echo $rightcolumn['fld_content_right_image']['url']; ?>" alt="<?php echo $rightcolumn['fld_content_right_image']['alt']; ?>" class="img-responsive" /></div>
		<?php endif; ?>
		<div class="post-link__inner">
			<h3 class="post-link__title"><?php the_title() ?></h3>
			<p class="post-link__body"><?php echo get_the_date(); ?></p>
			<div class="post-link__btn">Read news article</div>
		</div>
	</div>
</div>