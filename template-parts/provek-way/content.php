<?php
/**
 * Template part for displaying Provek Way
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Provek
 * @since 1.0
 * @version 1.0
 */

?>
<?php $provekway = new WP_Query(array('post_type' => 'provek', 'posts_per_page' => 4, 'orderby' => 'menu_order', 'order' => 'ASC')); ?>
<?php if ($provekway->have_posts()) : ?>
	<div class="section section--triangles section--triangles--auto">
		<div class="triangles top-right sml primary zindex1"></div>
		<div class="container">
			<div class="row pt-md pb-sm row-eq-height">	
				<?php while ($provekway->have_posts()) : $provekway->the_post(); 
					$image = get_field('fld_provek_way_image');
					$pagelink = get_field('fld_provek_way_link');
				?>
					<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
						<div class="panel-calltoaction">
							<?php if (!empty($pagelink)) { ?>
								<h3 class="heading-h6 color-secondary heading-keyline"><a href="<?php echo $pagelink['url']; ?>"><?php the_title(); ?></a></h3>
								<?php if (!empty($image)) { ?>
									<a href="<?php echo $pagelink['url']; ?>" class="thumbnail-theme"><img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>" class="img-responsive" /></a>
								<?php } ?>
								<?php if (!empty('fld_provek_way_excerpt')) { ?>
									<p><?php echo get_field('fld_provek_way_excerpt'); ?></p>
								<?php } ?>
								<?php if (!empty($pagelink['target'])) { ?>
									<a class="btn btn-default btn-theme btn-theme--primary panel-calltoaction__btn" href="<?php echo $pagelink['url']; ?>" target="<?php echo $pagelink['target']; ?>"><?php echo $pagelink['title']; ?></a>
								<?php } else { ?>
									<a class="btn btn-default btn-theme btn-theme--primary panel-calltoaction__btn" href="<?php echo $pagelink['url']; ?>"><?php echo $pagelink['title']; ?></a>
								<?php } ?>
							<?php } else { ?>
								<h3 class="heading-h6 color-secondary heading-keyline"><a href="/contact/"><?php the_title(); ?></a></h3>
								<?php if (!empty($image)) { ?>
									<a href="/contact/" class="thumbnail-theme"><img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>" class="img-responsive" /></a>
								<?php } ?>
								<?php if (!empty('fld_provek_way_excerpt')) { ?>
									<p><?php echo get_field('fld_provek_way_excerpt'); ?></p>
								<?php } ?>
								<a class="btn btn-default btn-theme btn-theme--primary panel-calltoaction__btn" href="/contact/">Contact us</a>
							<?php } ?>
						</div>
					</div>
				<?php endwhile; wp_reset_query(); ?>
			</div>
		</div>
	</div>
<?php endif; ?>