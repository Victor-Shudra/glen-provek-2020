<?php
/**
 * Template part for displaying Breadcrumb 
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Provek
 * @since 1.0
 * @version 1.0
 */

?>
<div class="section section--breadcrumb">
	<div class="container">
	  <div class="row">
	    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
	    	<?php 
	    		echo "<p id='breadcrumbs'><a href='/'>Provek</a> » <a href='/case-studies/'>Case studies</a>";
	    		if (function_exists('yoast_breadcrumb')) {
					yoast_breadcrumb(' » ', '');
				}
				echo "</p>";
	    	 ?>
	    </div>
	  </div>
	</div>
</div>