<?php
/**
 * Template part for displaying Default Download Content
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Provek
 * @since 1.0
 * @version 1.0
 */

?>

<?php 
	$downloadlink = get_field('fld_file_upload');
?>

<!-- CONTENT -->
<div class="section section--triangles">
	<!-- TRIANGLES -->
	<div class="triangles top-right xsml secondary zindex5"></div>
	<div class="triangles base-left sml primary zindex3"></div>
	<div class="triangles base-right lrg gray zindex1"></div>
	
	<div class="container">
		<!-- PAGE TITLE -->
		<div class="row pt-md pb-sm">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<h1 class="heading-h3 color-secondary">Thank you</h1>
			</div>
		</div>

		<!-- SUB-TITLES -->
		<div class="row pb-sm">
			<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
				<h3 class="heading-h6 color-secondary heading-keyline">Thank you for your purchase.</h3>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-5 col-md-offset-1 col-lg-5 col-lg-offset-1">
				<h3 class="heading-h6 color-secondary heading-keyline">Any Questions?</h3>
			</div>
		</div>

		<!-- DOWNLOADS -->
		<?php if (!empty($downloadlink)): ?>
			<div class="row pb-sm">
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
					<h3 class="heading-h4 color-secondary">Please click the link to download your eBook</h3>
					<a class="btn btn-default btn-theme btn-theme--primary" href="<?php echo $downloadlink['url']; ?>" target="_blank">Download</a>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-5 col-md-offset-1 col-lg-5 col-lg-offset-1">
					<p>Please contact us if you have any questions about your purchase.</p>
					<a class="btn btn-default btn-theme btn-theme--primary" href="/contact/" target="_blank">Contact us</a>
				</div>
			</div>
		<?php endif; ?>
	</div>
</div>