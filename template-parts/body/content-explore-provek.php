<?php
/**
 * Template part for displaying Single About Content
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Provek
 * @since 1.0
 * @version 1.0
 */

?>
<!-- CONTENT -->
<?php 
	$video = get_field('grp_about_video');
	$introduction = get_field('grp_about_intro');
	$trackrecord = get_field('grp_track_record');
?>

<div class="section section--triangles">
	<!-- VIDEO BANNER -->
	<div class="triangles top-right xsml secondary zindex5"></div>
	<?php if (!empty($video['fld_exploring_video_image'])): ?>
		<div class="top-video--img" style="background:url(<?php echo $video['fld_exploring_video_image']['url']; ?>) 0 0 no-repeat;background-size:cover;background-position:center;">
			<button type="button" class="btn btn-default btn-play" data-toggle="modal" data-target="#Watch"> PLAY </button>
		</div>
	<?php endif; ?>
	<div class="triangles base-left sml primary zindex3"></div>
	<div class="triangles base-right xlrg gray zindex1"></div>

	<div class="container">
		<!-- PAGE TITLE -->
		<div class="row pt-md pb-sm">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<h1 class="heading-h3 color-secondary"><?php the_title() ?></h1>
			</div>
		</div>

		<!-- BODY -->
		<?php if (!empty($introduction['fld_about_title'])): ?>
			<div class="row pb-sm">
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
					<h3 class="heading-h6 color-secondary heading-keyline"><?php echo $introduction['fld_about_title']; ?></h3> 
					<?php if (!empty($introduction['fld_about_excerpt'])): ?>
						<h3 class="heading-h4 color-secondary"><?php echo $introduction['fld_about_excerpt']; ?></h3>
					<?php endif; ?>
					<?php if (!empty($introduction['fld_about_body'])): ?>
						<div class="content"><?php echo $introduction['fld_about_body']; ?></div>
					<?php endif; ?>
					<?php if (isset($introduction['fld_about_linktopage']['url'])): ?>
						<?php if (!empty($introduction['fld_about_linktopage']['target'])): ?>
							<a class="btn btn-default btn-theme btn-theme--primary" href="<?php echo $introduction['fld_about_linktopage']['url']; ?>" target="<?php echo $introduction['fld_about_linktopage']['target']; ?>"><?php echo $introduction['fld_about_linktopage']['title']; ?></a>
						<?php else: ?>
							<a class="btn btn-default btn-theme btn-theme--primary" href="<?php echo $introduction['fld_about_linktopage']['url']; ?>"><?php echo $introduction['fld_about_linktopage']['title']; ?></a>
						<?php endif; ?>
					<?php endif; ?>
				</div>
			</div>
		<?php endif; ?>

		<!-- KEY PAGES -->
		<?php if (have_rows('rpt_about_page_links')): ?>
			<div class="row pb-sm row-eq-height">
				<?php $rowCounter = count(get_field('rpt_about_page_links')); ?>
				<?php while (have_rows('rpt_about_page_links')): the_row(); 
					$title = get_sub_field('fld_about_title');
					$excerpt = get_sub_field('fld_about_excerpt');
					$calltoaction = get_sub_field('fld_about_linktopage');
				?>
					<?php if ($rowCounter <= 2): ?>
						<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
					<?php elseif ($rowCounter > 2): ?>
						<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
					<?php endif; ?>
							<div class="category-calltoaction">
								<div class="triangles base-right med black opacity1 zindex1"></div>
								<?php if (isset($calltoaction['url'])) { ?>
									<?php if (!empty($calltoaction['target'])) { ?>
										<a class="category-calltoaction__link" href="<?php echo $calltoaction['url']; ?>" target="<?php echo $calltoaction['target']; ?>"></a>
									<?php } else { ?>
										<a class="category-calltoaction__link" href="<?php echo $calltoaction['url']; ?>"></a>
									<?php } ?>
								<?php } ?>
								<?php if (!empty($title)) { ?>
									<h3 class="category-calltoaction__desctitle"><?php echo $title; ?></h3>
								<?php } ?>
								<?php if (!empty($excerpt)) { ?>
									<p class="category-calltoaction__body"><?php echo $excerpt; ?><br><br><strong>Find out more</strong></p>
								<?php } ?>
							</div>
						</div>
				<?php endwhile; ?>
			</div>
		</div>
	<?php endif; ?>
</div>

<!-- TRACK RECORD -->
<?php if (!empty($trackrecord['fld_track_record_title'])): ?>
	<div class="section section--triangles">
	<div class="triangles top-left med gray zindex1"></div>
		<div class="container">
			<!-- PAGE TITLE -->
			<div class="row pt-sm pb-sm">
			<?php if (!empty($trackrecord['fld_track_record_img'])): ?>
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
					<div class="thumbnail-theme--borderonly"><img src="<?php echo $trackrecord['fld_track_record_img']['url']; ?>" alt="<?php echo $trackrecord['fld_track_record_img']['alt']; ?>" class="img-responsive" /></div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-5 col-md-offset-1 col-lg-5 col-lg-offset-1">
			<?php else: ?>
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<?php endif; ?>
					<h3 class="heading-h6 color-secondary heading-keyline"><?php echo $trackrecord['fld_track_record_title']; ?></h3>
					<?php if (!empty($trackrecord['fld_track_record_excerpt'])): ?>
						<h3 class="heading-h4 color-secondary"><?php echo $trackrecord['fld_track_record_excerpt']; ?></h3>
					<?php endif; ?>
					<?php if (!empty($trackrecord['fld_track_record_body'])): ?>
						<div class="content"><?php echo $trackrecord['fld_track_record_body']; ?></div>
					<?php endif; ?>
					<?php if (isset($trackrecord['fld_track_record_linktopage']['url'])): ?>
						<?php if (!empty($trackrecord['fld_track_record_linktopage']['target'])): ?>
							<a class="btn btn-default btn-theme btn-theme--primary" href="<?php echo $trackrecord['fld_track_record_linktopage']['url']; ?>" target="<?php echo $trackrecord['fld_track_record_linktopage']['target']; ?>"><?php echo $trackrecord['fld_track_record_linktopage']['title']; ?></a>
						<?php else: ?>
							<a class="btn btn-default btn-theme btn-theme--primary" href="<?php echo $trackrecord['fld_track_record_linktopage']['url']; ?>"><?php echo $trackrecord['fld_track_record_linktopage']['title']; ?></a>
						<?php endif; ?>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
<?php endif; ?>


<div class="modal fade" id="Watch" tabindex="-1" role="dialog" aria-labelledby="Watch" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<iframe width="650" height="450" src="<?php echo $video['fld_exploring_video_url']; ?>" style="border:0;width:100%;"></iframe>
	</div>
</div>