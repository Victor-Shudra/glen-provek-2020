<?php
/**
 * Template part for displaying Single Staff Content
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Provek
 * @since 1.0
 * @version 1.0
 */

?>
<!-- CONTENT -->
<?php 
	$image = get_field('fld_staff_image');
	$body = get_field('fld_staff_body');
//	$position = get_field('fld_staff_position');
	$position =  get_field('fld_staff_tel');
	$telephone = get_field('fld_staff_tel');
	$email = get_field('fld_staff_email');
?>

<div class="section section--triangles">
	<!-- TRIANGLES -->
	<div class="triangles top-right xsml secondary zindex5"></div>
	<div class="triangles base-left sml primary zindex3"></div>
	<div class="triangles base-right lrg gray zindex1"></div>

	<div class="container">
		<!-- PAGE TITLE -->
		<div class="row pt-md pb-sm">
			<?php if (!empty($image)) { ?>
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
			<?php } else { ?>
				<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
			<?php } ?>
					<h1 class="heading-h3 color-secondary"><?php the_title() ?></h1>
					<?php if (!empty($position)) { ?>
						<h3 class="heading-h4 color-secondary"><?php echo $position; ?></h3>
					<?php } ?>
				</div>
			<?php if (!empty($image)) { ?>
				<div class="col-xs-4 col-sm-2 col-sm-offset-4 col-md-2 col-md-offset-4 col-lg-2 col-lg-offset-4">
					<div class="thumbnail-theme thumbnail-theme--image"><img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" class="img-responsive" /></div>
				</div>
			<?php } ?>
		</div>

		<!-- BODY -->
		<?php if (!empty($body || $email)) { ?>
			<div class="row pb-sm">
				<?php if (!empty($email)) { ?>
					<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
				<?php } else { ?>		
					<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
				<?php } ?>
						<h3 class="heading-h6 color-secondary heading-keyline">Profile</h3>
						<?php if (!empty($body)) { ?>
							<div class="content"><?php echo $body; ?></div>
						<?php } ?>
					</div>
			<?php // if (!empty($email)) { ?>
				<?php 
                // kept
                if (false) { ?>
					<div class="col-xs-12 col-sm-12 col-md-5 col-md-offset-1 col-lg-5 col-lg-offset-1">
						<h3 class="heading-h6 color-secondary heading-keyline">Contact details</h3>
						<h3 class="heading-h4 color-secondary" style="margin: 0;"><span class="color-primary">T:</span> <?php echo $telephone; ?></h3>
						<h3 class="heading-h4 color-secondary" style="margin: 0;"><span class="color-primary">E:</span> <a href="mailto:<?php echo $email; ?>"><?php echo $email; ?></a></h3>
					</div>
				<?php } ?>
			</div>
		<?php } ?>
	</div>
</div>