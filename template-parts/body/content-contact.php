<?php
/**
 * Template part for displaying Contact Content
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Provek
 * @since 1.0
 * @version 1.0
 */

?>
<!-- CONTENT -->
<?php 
	$leftcolumn = get_field('grp_content_left');
	$rightcolumn = get_field('grp_content_right');
?>

<div class="section section--triangles">
	<!-- TRIANGLES -->
	<div class="triangles top-right xsml secondary zindex5"></div>
	<div class="triangles base-left sml primary zindex3"></div>
	<div class="triangles base-right lrg gray zindex1"></div>

	<div class="container">
		<!-- PAGE TITLE -->
		<div class="row pt-md pb-sm">
			<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
				<h1 class="heading-h3 color-secondary"><?php the_title() ?></h1>
				<h3 class="heading-h4 color-secondary"><?php echo $leftcolumn['fld_content_left_excerpt']; ?></h3>
			</div>
		</div>

		<!-- BODY -->
		<?php if (!empty($leftcolumn['fld_content_left_excerpt'])): ?>
			<div class="row pb-sm">
				<?php if (!empty($rightcolumn['fld_content_right_form'])): ?>
					<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
				<?php else: ?>		
					<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
				<?php endif; ?>
					<h3 class="heading-h6 color-secondary heading-keyline">Get in touch today</h3>
					<?php if (!empty($leftcolumn['fld_content_left_tel'])): ?>
						<h3 class="heading-h4 color-secondary" style="margin: 0;"><span class="color-primary">T:</span> <?php echo $leftcolumn['fld_content_left_tel']; ?></h3>
					<?php endif; ?>
					<?php if (!empty($leftcolumn['fld_content_left_email'])): ?>
						<h3 class="heading-h4 color-secondary" style="margin: 0;"><span class="color-primary">E:</span> <a href="mailto:<?php echo $leftcolumn['fld_content_left_email']; ?>"><?php echo $leftcolumn['fld_content_left_email']; ?></a></h3>
					<?php endif; ?>
					<h3 class="heading-h6 color-secondary heading-keyline">Where we are?</h3>
					<?php if (!empty($leftcolumn['fld_content_left_address'])): ?>
						<p><strong><?php echo $leftcolumn['fld_content_left_address']; ?></strong></p>
					<?php endif; ?>

					<?php if (!empty($leftcolumn['fld_content_left_map'])): ?>
						<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA0ikNIMWuAeeuAH9MEFPIMhyxYE16JV34"></script>
						<noscript>This website requires Javascript. Please enable this to improve your experience</noscript>
						<div class="acf-map">
							<div class="marker" data-lat="<?php echo $leftcolumn['fld_content_left_map']['lat']; ?>" data-lng="<?php echo $leftcolumn['fld_content_left_map']['lng']; ?>"></div>
						</div>
					<?php endif; ?>
					<?php if (isset($leftcolumn['fld_content_left_linktopage']['url'])): ?>
						<?php if (!empty($leftcolumn['fld_content_left_linktopage']['target'])): ?>
							<a class="btn btn-default btn-theme btn-theme--primary" href="<?php echo $leftcolumn['fld_content_left_linktopage']['url']; ?>" target="<?php echo $leftcolumn['fld_content_left_linktopage']['target']; ?>"><?php echo $leftcolumn['fld_content_left_linktopage']['title']; ?></a>
						<?php else: ?>
							<a class="btn btn-default btn-theme btn-theme--primary" href="<?php echo $leftcolumn['fld_content_left_linktopage']['url']; ?>"><?php echo $leftcolumn['fld_content_left_linktopage']['title']; ?></a>
						<?php endif; ?>
					<?php endif; ?>
				</div>
				<?php if (!empty($rightcolumn['fld_content_right_form'])): ?>
					<div class="col-xs-12 col-sm-12 col-md-5 col-md-offset-1 col-lg-5 col-lg-offset-1">
						<?php echo do_shortcode($rightcolumn['fld_content_right_form']); ?>
					</div>
				<?php endif; ?>
			</div>
		<?php endif; ?>
	</div>
</div>