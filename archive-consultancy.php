<?php 

/**
 * Template Name: Consultancy Listing
 *
 * A custom page template without Primary and Secondary sidebars.
 *
 * The "Template Name:" bit above allows this to be selectable
 * from a dropdown menu on the edit page screen.
 *
 * @package WordPress
 * @subpackage Provek
 * @since Provek 1.0
 */

get_header(); ?>

	<!-- BREADCRUMB -->
	<?php get_template_part('template-parts/breadcrumb/content'); ?>

	<!-- BODY CONTENT -->		
	<?php get_template_part('template-parts/taxonomy/content', 'consultancy'); ?>

	<!-- CLIENTS -->
	<?php get_template_part('template-parts/carousels/content', 'client'); ?>

	<!-- THE PROVEK WAY -->
	<?php get_template_part('template-parts/provek-way/content'); ?>
	
<?php get_footer(); ?>