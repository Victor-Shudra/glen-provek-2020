<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> 
<html class="no-js" lang=""> <!--<![endif]-->
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
  <meta name="format-detection" content="telephone=no">
  <title><?php wp_title( '', true, 'right' ); ?></title>

  <!-- GOOGLE ANALYTICS -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-7212738-1"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-7212738-1');
  </script>

  <?php wp_head(); ?>
  <!--[if lte IE 9]>
    <link rel="stylesheet" type="text/css" href="/wp-content/themes/provektheme/dist/css/ie9.css" />
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->
</head>
<body <?php body_class(); ?>>

  <!--[if lt IE 8]>
    <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
  <![endif]-->

  <div id="header-wrap">
    <div class="section">
      <nav class="navbar navbar-default navigation-block" role="navigation">
        <div class="container-custom">

          <div class="navbar-header navigation-block__head">

            <a href="<?php echo home_url(); ?>" class="logo">
              <img src="<?php echo get_stylesheet_directory_uri(); ?>/logo.png" alt="<?php echo get_bloginfo('name'); ?>" class="img-responsive" />
            </a>

            <button type="button" class="navbar-toggle collapsed navigation-block__toggle" data-toggle="collapse" data-target="#primary-navigation" aria-expanded="false">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar top-bar"></span>
              <span class="icon-bar middle-bar"></span>
              <span class="icon-bar bottom-bar"></span>
            </button>

          </div>

          <div class="navigation-block__nav">
            <?php 
              if(is_active_sidebar('contact-header')) {
                dynamic_sidebar('contact-header');
              }
             ?>

            <?php
              wp_nav_menu( array(
                'theme_location'    => 'primary',
                'depth'             => 3,
                'container'         => 'div',
                'container_class'   => 'collapse navbar-collapse navigation-block__collapse',
                'container_id'      => 'primary-navigation',
                'menu_class'        => 'nav navbar-nav navigation-block',
                'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
                'walker'            => new WP_Bootstrap_Navwalker())
              );
            ?>

            <div class="form-block form-block--nav-search">
              <form role="search" method="get" id="searchform" class="searchform" action="<?php echo home_url( '/' ); ?>">
                <label for="s">Search</label>
                <input type="text" class="form-block__input" placeholder="<?php echo esc_attr_x( 'Search Content...', 'placeholder' ) ?>" value="<?php echo get_search_query() ?>" name="s" id="s">
              </form>
            </div>

          </div>
        </div>
      </nav>
    </div>
  </div>

<div id="content-wrap">