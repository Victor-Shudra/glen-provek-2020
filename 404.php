<?php get_header(); ?>

	<!-- BREADCRUMB -->
	<?php get_template_part('template-parts/breadcrumb/content'); ?>
	
	<!-- BODY CONTENT -->
	<div class="section section--triangles">
		<!-- TRIANGLES -->
		<div class="triangles top-right xsml secondary zindex5"></div>
		<div class="triangles base-left sml primary zindex3"></div>
		<div class="triangles base-right lrg gray zindex1"></div>

		<div class="container">
			<!-- PAGE TITLE -->
			<div class="row pt-sm">
				<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
					<h1 class="heading-h3 color-secondary">404</h1>
					<h3 class="heading-h4 color-secondary">The page you are looking for, can't be found.</h3>
				</div>
			</div>

			<!-- BODY -->
			<div class="row pb-sm">
				<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
					<p>Please <a href="/" class="btn-theme color-primary">click here</a> to return to the homepage.</p>
				</div>
			</div>
		</div>
	</div>

	<!-- CLIENTS -->
	<?php get_template_part('template-parts/carousels/content', 'client'); ?>

	<!-- THE PROVEK WAY -->
	<?php get_template_part('template-parts/provek-way/content'); ?>

<?php get_footer(); ?>