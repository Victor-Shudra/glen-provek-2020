(function( $ ) {

		function new_map( $el ) {
		// var
		var $markers = $el.find('.marker');
		// vars
		var args = {
			zoom		: 10,
			center		: new google.maps.LatLng(0, 0),
			mapTypeId	: google.maps.MapTypeId.ROADMAP
		};
		// create map	        	
		var map = new google.maps.Map( $el[0], args);
		// add a markers reference
		map.markers = [];
		// add markers
		$markers.each(function(){
	    	add_marker( $(this), map );
		});
		// center map
		center_map( map );
		// return
		return map;
	}

	/*
	*  add_marker
	*
	*  This function will add a marker to the selected Google Map
	*
	*  @type	function
	*  @date	8/11/2013
	*  @since	4.3.0
	*
	*  @param	$marker (jQuery element)
	*  @param	map (Google Map object)
	*  @return	n/a
	*/

	function add_marker( $marker, map ) {
		// var
		var latlng = new google.maps.LatLng( $marker.attr('data-lat'), $marker.attr('data-lng') );
		// create marker
		var marker = new google.maps.Marker({
			position	: latlng,
			map			: map
		});
		// add to array
		map.markers.push( marker );
		// if marker contains HTML, add it to an infoWindow
		if( $marker.html() )
		{
			// create info window
			var infowindow = new google.maps.InfoWindow({
				content		: $marker.html()
			});

			// show info window when marker is clicked
			google.maps.event.addListener(marker, 'click', function() {

				infowindow.open( map, marker );
			});
		}
	}

	/*
	*  center_map
	*
	*  This function will center the map, showing all markers attached to this map
	*
	*  @type	function
	*  @date	8/11/2013
	*  @since	4.3.0
	*
	*  @param	map (Google Map object)
	*  @return	n/a
	*/

	function center_map( map ) {
		// vars
		var bounds = new google.maps.LatLngBounds();
		// loop through all markers and create bounds
		$.each( map.markers, function( i, marker ){
			var latlng = new google.maps.LatLng( marker.position.lat(), marker.position.lng() );
			bounds.extend( latlng );
		});
		// only 1 marker?
		if( map.markers.length == 1 )
		{
			// set center of map
		    map.setCenter( bounds.getCenter() );
		    map.setZoom( 10 );
		}
		else
		{
			// fit to bounds
			map.fitBounds( bounds );
		}
	}

	/*
	*  document ready
	*
	*  This function will render each map when the document is ready (page has loaded)
	*
	*  @type	function
	*  @date	8/11/2013
	*  @since	5.0.0
	*
	*  @param	n/a
	*  @return	n/a
	*/
	// global var
	var map = null;


	$(document).ready(function() {
		// NAVIGATION
		var doc = document.documentElement;
		doc.setAttribute('data-useragent', navigator.userAgent);
		
		$("#menu-main-navigation").children(".menu-item--level-0").find("a").click(function(e) {
			var menuItemParent = $(this).parent(".menu-item--level-0");

			// SEARCH
			if($(menuItemParent).hasClass("nav-megamenu__trigger")) {
				$(menuItemParent).toggleClass("menu-open");
				$(menuItemParent).siblings(".menu-item--level-0").removeClass("menu-open");
				$(".form-block--nav-search").toggleClass("search-open");
				e.stopPropagation();
				return false;
			}

			// ADD CLASS TO SEARCH BOX
			if($("nav-megamenu__trigger").hasClass("menu-open")) {
				$(".form-block--nav-search").addClass("search-open");
				e.stopPropagation();
				return false;
			}

			// MENU
			if($(menuItemParent).hasClass("menu-item-has-children")) {
				$(menuItemParent).toggleClass("menu-open");
				$(".form-block--nav-search").removeClass("search-open");
				$(menuItemParent).siblings(".menu-item--level-0").removeClass("menu-open");
				//e.stopPropagation();
				return false;
			}
		});

		//BODY CLOSE
		$('body').click(function() {
			$("#menu-main-navigation li").removeClass("menu-open");
			//$(".form-block--nav-search").removeClass("search-open");
		});

		// ARCHIVE TOGGLE
		$('.archive-year').click(function() {
			// $('.archive-month').removeClass('open');
    		var month = $(this).find('.archive-month').toggleClass('open');
    		var year = $(this).toggleClass('parent-open');
			// if($(this).hasClass('archive-year')){
			//     return false;
			// }
			//e.preventDefault();
			//return false;
		});
		
		// CONTACT FORM 7 HIDE FORM ON SUBMIT 
		document.addEventListener('wpcf7mailsent', function(event) {
		    document.getElementById("form-hide").style.display = 'none';
		}, false);	

		$('.accreditation a').click(function(e) {
			e.preventDefault();
			window.open(this.href)
		});

		$('.social a').click(function(e) {
			e.preventDefault();
			window.open(this.href)
		});

		$('.acf-map').each(function(){
			// create map
			map = new_map( $(this) );
		});

	});

})(jQuery);