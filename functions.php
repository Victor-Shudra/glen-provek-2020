<?php
if (function_exists('register_sidebar')) {
  register_sidebar(array(
    'name' => 'Contact Details',
    'id' => 'contact-header',
    'description' => 'The contact details just before the Navigation area',
    'before_widget' => '<div class="menu-item--contact-details">',
    'after_widget' => '</div>'
  ));
  register_sidebar(array(
    'name' => 'Pre-Footer',
    'id' => 'pre-footer',
    'description' => 'The banner image and text in the footer',
    'before_widget' => '',
    'after_widget' => '',
    'before_title'  => '<h3 class="heading-h6 color-white heading-keyline">',
    'after_title'   => '</h3>'
  ));
  register_sidebar(array(
    'name' => 'Footer One',
    'id' => 'footer-one',
    'description' => 'The left hand column on the Footer area',
    'before_widget' => '',
    'after_widget' => '',
    'before_title'  => '<h3 class="heading-h6 color-secondary heading-keyline">',
    'after_title'   => '</h3>'
  ));
  register_sidebar(array(
    'name' => 'Footer Two',
    'id' => 'footer-two',
    'description' => 'The right hand column on the Footer area',
    'before_widget' => '<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4"><div class="accreditation">',
    'after_widget' => '</div></div>'
  ));
  register_sidebar(array(
    'name' => 'Social Icons',
    'id' => 'social-icons',
    'description' => 'Social links',
    'before_widget' => '<div class="social">',
    'after_widget' => '</div>',
    'before_title'  => '<h3 style="display:none;">',
    'after_title'   => '</h3>'
  ));
  register_sidebar(array(
    'name' => 'Blog Sidebar',
    'id' => 'blog-sidebar',
    'description' => 'The right hand column on the Blog area',
    'before_widget' => '',
    'after_widget' => ''
  ));
  register_sidebar(array(
    'name' => 'Sidebar',
    'id' => 'sidebar',
    'description' => 'The right hand column on All pages',
    'before_widget' => '',
    'after_widget' => ''
  ));
  register_sidebar(array(
    'name' => 'Sitemap',
    'id' => 'sitemap',
    'description' => 'This is for SEO purposes. This runs off the main navigation.',
    'before_widget' => '',
    'after_widget' => ''
  ));
}

/* GOOGLE MAP API */ /* CHANGE TO CLIENT API ON LAUNCH */
// <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA0ikNIMWuAeeuAH9MEFPIMhyxYE16JV34"></script>
// function my_acf_google_map_api( $api ){
//   $api['key'] = 'AIzaSyA0ikNIMWuAeeuAH9MEFPIMhyxYE16JV34';
//   return $api;
  
// }
// add_filter('acf/fields/google_map/api', 'my_acf_google_map_api');


/* ACCESSIBILITY FIXES */

// YOAST BREADCRUMB FIX
function bybe_crumb_v_fix ($link_output) {
  $link_output = preg_replace(array('#<span xmlns:v="http://rdf.data-vocabulary.org/\#">#','#<span typeof="v:Breadcrumb"><a href="(.*?)" .*?'.'>(.*?)</a></span>#','#<span typeof="v:Breadcrumb">(.*?)</span>#','# property=".*?"#','#</span>$#'), array('','<span itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="$1" itemprop="url"><span itemprop="title">$2</span></a></span>','<span itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title">$1</span></span>','',''), $link_output);
  return $link_output;
}
add_filter ('wpseo_breadcrumb_output','bybe_crumb_v_fix');

// REMOVE TEXT/JAVASCRIPT FROM <SCRIPT>
// function clean_script_tag_space($input) {
//   $input = str_replace("type='text/javascript' ", '', $input);
//   return str_replace("'", '"', $input);
// }
// add_filter('script_loader_tag', 'clean_script_tag_space');

// APPEND <NOSCRIPT> TO <SCRIPT>
// function add_noscript_tag($tag)
// {
// $noScript = '<noscript>This website requires Javascript. Please enable this to improve your experience</noscript>';
// return str_replace('</script>', '</script>'.$noScript, $tag);
// }
// // Hook it into WP
// add_filter('script_loader_tag', 'add_noscript_tag');


/* CHILD THEME SCRIPTS */
function provektheme_styles() {
    $parent_style = 'basetheme';
    wp_enqueue_style($parent_style, get_template_directory_uri() . '/style.css' ); // PARENT THEME
    wp_enqueue_style('provektheme', get_stylesheet_directory_uri() . '/dist/css/style.min.css', array($parent_style), wp_get_theme()->get('Version')); // CHILD THEME CSS
    //wp_enqueue_script('provektheme', get_stylesheet_directory_uri() . '/dist/js/scripts.min.js', array(), '1.0.0', true);  // CHILD THEME JS
}
add_action( 'wp_enqueue_scripts', 'provektheme_styles' );


/* CUSTOM SEARCH */
function wds_cpt_search( $query ) {
    if ( is_search() && $query->is_main_query() && $query->get( 's' ) ){
        $query->set('post_type', array('page', 'training', 'case_studies', 'assessment', 'consultancy', 'pm_channel', 'provek', 'staff', 'shop'));
    }
    return $query;
};
add_filter('pre_get_posts', 'wds_cpt_search');

/* BOOTSTRAP MOBILE NAVIGATION */
require_once('wp_bootstrap_navwalker.php');

/* REGISTER MENUS */
register_nav_menus( array(  
  'primary' => __( 'Primary Navigation', 'provektheme' ),   
  'footer' => __('Footer Navigation', 'provektheme')  
) );

/* ADMIN LOGO */
function custom_loginlogo() {
  echo '<style type="text/css">
    h1 a {width:100% !important;height:70px !important;background-size:auto !important;background-image: url(wp-content/themes/provektheme/admin-logo.png) !important; }
  </style>';
}
add_action('login_head', 'custom_loginlogo');

/* PAGINATION - REMOVE SCREEN READER TEXT */
function sanitize_pagination($content) {
    $content = preg_replace('#<h2.*?>(.*?)<\/h2>#si', '', $content);
    return $content;
}
add_action('navigation_markup_template', 'sanitize_pagination');

/* PAGINATION UNUSED */
function pagination_bar( $custom_query ) {
  $total_pages = $custom_query->max_num_pages;
  $big = 999999999;

  if ($total_pages > 1){
      $current_page = max(1, get_query_var('paged'));

      echo paginate_links(array(
          'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
          'format' => '?paged=%#%',
          'current' => $current_page,
          'total' => $total_pages,
      ));
  }
}

/* SVG UPLOAD UNUSED */
// function allow_svgimg_types($mimes) {
//   $mimes['svg'] = 'image/svg+xml';
//   return $mimes;
// }
// add_filter('upload_mimes', 'allow_svgimg_types');

/* IMAGE COMPRESSION */
// add_theme_support( 'post-thumbnails' );
// add_image_size( 'list-image size', 360, 113 );
// add_image_size( 'triangle-image size', 610, 610 );

/* DASHBOARD ICONS */
function replace_admin_menu_icons_css() {
    ?>
    <style>
      .menu-icon-training .dashicons-admin-post:before,
  	  .menu-icon-assessment .dashicons-admin-post:before,
  	  .menu-icon-consultancy .dashicons-admin-post:before,
   	  .menu-icon-pm_channel .dashicons-admin-post:before {
        content: "\f105";
      }
      .menu-icon-case_studies .dashicons-admin-post:before {
        content: "\f318";
      }
      .menu-icon-testimonial .dashicons-admin-post:before {
        content: "\f125";
      }
      .menu-icon-staff .dashicons-admin-post:before {
        content: "\f307";
      }
      .menu-icon-shop .dashicons-admin-post:before {
        content: "\f174";
      }
      .menu-icon-downloads .dashicons-admin-post:before {
        content: "\f330";
      }
    </style>
    <?php
}
add_action( 'admin_head', 'replace_admin_menu_icons_css' );


function bc_register_my_taxes() {

	/**
	 * Taxonomy: Staff Sections.
	 */

	$labels = array(
		"name" => __( "Staff Sections", "provek" ),
		"singular_name" => __( "Staff Section", "provek" ),
	);

	$args = array(
		"label" => __( "Job Categories", "provek" ),
		"labels" => $labels,
		"public" => true,
		"publicly_queryable" => false,
		"hierarchical" => true,
		"show_ui" => true,
		"show_in_menu" => true,
		"show_in_nav_menus" => false,
		"query_var" => true,
		"rewrite" => array( 'slug' => 'staff_section', 'with_front' => true, ),
		"show_admin_column" => true,
		"show_in_rest" => true,
		"rest_base" => "staff_section",
		"rest_controller_class" => "WP_REST_Terms_Controller",
		"show_in_quick_edit" => true,
		);
	register_taxonomy( "staff_section", array( "staff" ), $args );
}
add_action( 'init', 'bc_register_my_taxes' );
