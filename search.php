<?php 

/**
 * Template Name: Search Listing
 *
 * A custom page template without Primary and Secondary sidebars.
 *
 * The "Template Name:" bit above allows this to be selectable
 * from a dropdown menu on the edit page screen.
 *
 * @package WordPress
 * @subpackage Provek
 * @since Provek 1.0
 */


get_header(); ?>

	<!-- BREADCRUMB -->
	<?php get_template_part('template-parts/breadcrumb/content'); ?>

	<!-- SEARCH LISTING -->
	<div class="section section--triangles">
		<!-- TRIANGLES -->
		<div class="triangles top-right xsml secondary zindex5"></div>
		<div class="triangles base-left sml primary zindex3"></div>
		<div class="triangles base-right lrg gray zindex1"></div>

		<div class="container">
			<div class="row pt-md pb-sm">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<h1 class="heading-h3 color-secondary">Search results for: <?php echo get_search_query(); ?></h1>
				</div>
			</div>
		</div>

		<!-- CONTENT -->
		<div class="container">
			<div class="row pb-sm">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<div class="row pb-sm">
						<?php if ( have_posts() ) : ?>
							<?php while (have_posts()) : the_post(); ?>
								<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
									<h3 class="heading-h4 color-secondary"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
									<!-- <a class="btn-theme color-primary" href="<?php //the_permalink(); ?>">Read more</a> -->
								</div>
							<?php endwhile; ?>
						<?php else: ?>
							<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
								<h3 class="heading-h4 color-secondary">Sorry, no results are available</h3>
								<p>Please <a href="/" class="btn-theme color-primary">click here</a> to return to the homepage.</p>
							</div>
						<?php endif; ?>	
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- CLIENTS -->
	<?php get_template_part('template-parts/carousels/content', 'client'); ?>

	<!-- THE PROVEK WAY -->
	<?php get_template_part('template-parts/provek-way/content'); ?>

<?php get_footer(); ?>