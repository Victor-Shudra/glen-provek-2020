<?php get_header(); ?>

	<!-- BREADCRUMB -->
	<?php get_template_part('template-parts/breadcrumb/content'); ?>

	<!-- STAFF BODY CONTENT -->
	<?php get_template_part('template-parts/body/content', 'staff-member'); ?>

	<!-- THE PROVEK WAY -->
	<?php get_template_part('template-parts/provek-way/content'); ?>

<?php get_footer(); ?>