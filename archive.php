<?php 

/**
 * Template Name: Post Listing
 *
 * A custom page template without Primary and Secondary sidebars.
 *
 * The "Template Name:" bit above allows this to be selectable
 * from a dropdown menu on the edit page screen.
 *
 * @package WordPress
 * @subpackage Provek
 * @since Provek 1.0
 */

get_header(); ?>

	<!-- BREADCRUMB -->
	<?php get_template_part('template-parts/breadcrumb/content'); ?>

	<!-- BODY CONTENT -->
	<div class="section section--triangles">
		<!-- TRIANGLES -->
		<div class="triangles top-right xsml secondary zindex5"></div>
		<div class="triangles base-left sml primary zindex3"></div>
		<div class="triangles base-right lrg gray zindex1"></div>

		<!-- PAGE TITLE -->
		<div class="container">
			<div class="row pt-md pb-sm">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<h1 class="heading-h3 color-secondary"><?php the_archive_title(); ?></h1>
				</div>
			</div>

			<!-- NEWS LISTING -->
			<div class="row pb-sm">
				<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
					<div class="row row-eq-height">
						<?php $args = array(
				            'posts_per_page'    => -1,
				            'post_type'         => 'post',
				            'post_status'       => 'publish',
				        );

				        // SEARCH CATEGORRY
				        if ( $_GET['cat'] ) {
				            $args['tax_query'] = array(
				                array(
				                    'taxonomy' => 'category',
				                    'field'    => 'term_id',
				                    'terms'    => $_GET['cat'],
				                ),
				            );
				        }

				        // SEARCH KEYWORD
				        if ( $_GET['search'] ) {
				            $args['s'] = $_GET['search'];
				        }

	        			$the_query = new WP_Query( $args );

				        if ( $the_query->have_posts() ) :
				            while ( $the_query->have_posts() ) : $the_query->the_post();
				            	get_template_part('template-parts/post/content', get_post_format());
				            endwhile;
				        else : ?>
				        	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				        		<h3 class="heading-h4 color-secondary">Sorry, no results are available</h3>
								<p>Please <a href="/news/" class="link-theme color-primary">click here</a> to reset the filter.</p>
							</div>
				        <?php endif; ?>

				        <?php wp_reset_postdata(); ?>
					</div>
					<?php if (have_posts()) { ?>
						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<?php
									the_posts_pagination(array(
										'mid_size'  => 2,
										'prev_text' => __('&laquo;', 'textdomain'),
										'next_text' => __('&raquo;', 'textdomain'),
									));
								?>
							</div>
						</div>
					<?php } ?>
				</div>

				<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
					<h3 class="heading-h6 color-secondary heading-keyline">Looking for something specific?</h3>

					<!-- FILTER  -->
					<!-- You searched within:
					<?php
				        //if ( $_GET['cat'] ) {
				            //$cat = get_term( $_GET['cat'], 'category' );
				            //echo $cat->name;
				        //} else {
				        	//echo "All categories";
				        //}
				    ?> -->

				    <div class="form-block form-block--searchandfilter">
						<?php $categories = get_terms( 'category'); ?>
						<form method="get">
							<select name="cat">
								<option value="">All categories</option>
							<?php foreach ( $categories as $category ) : ?>
								<option value="<?php echo $category->term_id; ?>" <?php if ( $_GET['cat'] && $_GET['cat'] == $category->term_id ) echo 'selected'; ?>>
									<?php echo $category->name; ?>
								</option>
							<?php endforeach; ?>
							</select>

							<label for="txtSearch">
								<input id="txtSearch" type="search" name="search" value="<?php echo $_GET['search']; ?>" placeholder="Search Keywords..." />
							</label>

							<button type="submit" class="btn btn-default btn-theme btn-theme--primary">Apply Filter</button>
						</form>
					</div>
								
					<h3 class="heading-h6 color-secondary heading-keyline">Archive</h3>

					<?php  
						global $wpdb;
						$limit = 0;
						$year_prev = null;
						$months = $wpdb->get_results("SELECT DISTINCT MONTH( post_date ) AS month ,  YEAR( post_date ) AS year, COUNT( id ) as post_count FROM $wpdb->posts WHERE post_status = 'publish' and post_date <= now( ) and post_type = 'post' GROUP BY month , year ORDER BY post_date DESC");
					?>
					
					<ul class="archive">
						<?php foreach($months as $month) : $year_current = $month->year; ?>
							<?php if ($year_current != $year_prev): ?>
								<?php if ($year_prev!==null): ?>
									</li>
								</ul>
								<?php endif; ?>
								<li class="archive-year"><!-- <a href="<?php //bloginfo('url') ?>/<?php //echo $month->year; ?>/"> --><a href="#"><?php echo $month->year; ?> Archive</a><!-- </a> -->
									<ul class="archive-month">
	    					<?php else: ?>
						    <li><a href="<?php bloginfo('url') ?>/<?php echo $month->year; ?>/<?php echo date("m", mktime(0, 0, 0, $month->month, 1, $month->year)) ?>"><?php echo date_i18n("F Y", mktime(0, 0, 0, $month->month, 1, $month->year)) ?></a></li>
							<?php endif; ?>
						<?php $year_prev = $year_current; ?>
						<?php endforeach; ?>
						</ul>
					</ul>

				</div>
			</div>
		</div>
	</div>

	<!-- THE PROVEK WAY -->
	<?php get_template_part('template-parts/provek-way/content'); ?>

<?php get_footer(); ?>