<?php get_header(); ?>

	<!-- BREADCRUMB -->
	<?php get_template_part('template-parts/breadcrumb/content'); ?>

	<!-- DOWNLOAD BODY CONTENT -->
	<?php get_template_part('template-parts/body/content', 'download'); ?>

	<!-- CLIENTS -->
	<?php //get_template_part('template-parts/carousels/content', 'client'); ?>

	<!-- THE PROVEK WAY -->
	<?php get_template_part('template-parts/provek-way/content'); ?>

<?php get_footer(); ?>