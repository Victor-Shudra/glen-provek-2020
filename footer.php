	<!-- </div></div> -->

	<?php if(is_active_sidebar('pre-footer')) { ?>
		<div class="section section--triangles section--expert">
			<div class="triangles base-right lrg black opacity1 zindex3"></div>
			<div class="triangles base-left xlrg primary zindex1"></div>
			<div class="container">
				<div class="row pt-lg pb-lg">
					<div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">
						<div class="color-white"><?php dynamic_sidebar('pre-footer'); ?>
							<a class="btn btn-default btn-theme btn-theme--white" href="/contact/">Request a call back</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	<?php } ?>

	<?php if(is_active_sidebar('footer-one') && ('footer-one')) { ?>
		<div class="section">
			<div class="container">
				<div class="row pt-sm pb-sm">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
								<?php dynamic_sidebar('footer-one'); ?>
							</div>
							<div class="col-xs-12 col-sm-12 col-md-6 col-md-offset-2 col-lg-6 col-lg-offset-2 row-flex">
								<div class="row">
									<?php dynamic_sidebar('footer-two'); ?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	<?php } ?>

	<div class="section" style="background-color:#B5B5B5;">
		<div class="container">
			<div class="row pt-sm pb-sm">
				<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
					<div class="resource">
						<div><a href="<?php echo home_url(); ?>">&copy; <?php print date("Y"); ?> <?php echo get_bloginfo('name'); ?></a></div>
						<?php wp_nav_menu( array( 'theme_location' => 'footer' ) ); ?>
						<?php if(is_front_page()) { ?>
							<a href="http://www.rouge-media.com" target="_blank">Website created by Rouge Media</a>
						<?php } else { ?>
							<a href="http://www.rouge-media.com" target="_blank">Website created by Rouge Media</a>
						<?php } ?>
					</div>
				</div>
				<?php if(is_active_sidebar('social-icons')) { ?>
					<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
						<?php dynamic_sidebar('social-icons'); ?>
					</div>
				<?php } ?>
			</div>
		</div>
	</div>

	<?php wp_footer(); ?>
	<noscript>This website requires Javascript. Please enable this to improve your experience</noscript>
</div>
</body>
</html>