<?php 

/**
 * Template Name: Front Page Template
 *
 * A custom page template without Primary and Secondary sidebars.
 *
 * The "Template Name:" bit above allows this to be selectable
 * from a dropdown menu on the edit page screen.
 *
 * @package WordPress
 * @subpackage Provek
 * @since Provek 1.0
 */

get_header(); ?>

	<!-- CAROUSEL -->
	<?php $first = true; ?>
	<div id="homepageCarousel" class="carousel carousel-block slide carousel-block--homepage" data-ride="carousel">
		<div class="carousel-inner carousel-block__inner section--triangles">

			<div class="triangles base-right med-carousel white opacity1 zindex5"></div>
			<?php if (have_rows('rpt_homepage_carousel')): ?>
				<?php while (have_rows('rpt_homepage_carousel')): the_row();
					$image = get_sub_field('fld_carousel_image');
					$pagelink = get_sub_field('fld_carousel_linktopage');
				?>
				<?php if (!empty($image)) { ?>
					<div class="section section--overlay item <?php echo $first ? ' active' : ''; ?> carousel-block__item" style="background-image:url(<?php echo $image['url']; ?>);">
				<?php } else { ?>
					<div class="section item<?php echo $first ? ' active' : ''; ?> carousel-block__item" style="background-color:#E00051;">
				<?php } ?>		
					<!-- CAROUSEL CONTENT -->
					<div class="container">
						<div class="row">
							<div class="col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
								<?php if (!empty('fld_carousel_title')) { ?>
									<h<?php print $first ? 1 : 2; ?> class="heading-h1 color-white heading-keyline"><?php echo get_sub_field('fld_carousel_title'); ?></h<?php print $first ? 1 : 2; ?>>
								<?php } ?>
								<?php if (!empty('fld_carousel_excerpt')) { ?>
									<p class="heading-h6 color-white"><?php echo get_sub_field('fld_carousel_excerpt'); ?></p>
								<?php } ?>

								<?php if (!empty($pagelink)) { ?>
									<?php if (!empty($pagelink['target'])) { ?>
										<a class="btn btn-default btn-theme btn-theme--primary" href="<?php echo $pagelink['url']; ?>" target="<?php echo $pagelink['target']; ?>"><?php echo $pagelink['title']; ?></a>
									<?php } else { ?>
										<a class="btn btn-default btn-theme btn-theme--primary" href="<?php echo $pagelink['url']; ?>"><?php echo $pagelink['title']; ?></a>
									<?php } ?>
								<?php } ?>

							</div>
						</div>
					</div>
					<div class="overlay--opacity"></div>
					<!-- CAROUSEL CONTENT END -->
				</div>
				<?php $first = false; ?>
				<?php endwhile; ?>
			<?php endif; ?>
			<!-- CONTROLS -->
			<a class="btn btn-default carousel-block__btn carousel-block__btn-left color-white" href="#homepageCarousel" data-slide="prev">Previous</a>
			<a class="btn btn-default carousel-block__btn carousel-block__btn-right color-white" href="#homepageCarousel" data-slide="next">Next</a>
		</div>
	</div>

	<!-- INTRODUCTION -->
	<?php $introduction = get_field('grp_homepage_intro');?>
	<?php if (!empty($introduction['fld_intro_title'])): ?>
	<div class="section section--triangles section--triangles--auto">
		<div class="triangles base-left med primary zindex3"></div>
		<div class="triangles base-right xlrg gray zindex1"></div>

		<div class="container">
			<div class="row pt-lg">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<h3 class="heading-h6 color-secondary heading-keyline"><?php echo $introduction['fld_intro_title']; ?></h3>
				</div>
			</div>
			<div class="row pb-md">
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
					<?php if (!empty($introduction['fld_intro_excerpt'])): ?>
						<h3 class="heading-h2 color-secondary"><?php echo $introduction['fld_intro_excerpt']; ?></h3>
					<?php endif; ?>
					<?php if (!empty($introduction['fld_intro_highlight'])): ?>
						<h3 class="heading-h2 color-primary" style="margin-top: 0;"><?php echo $introduction['fld_intro_highlight']; ?></h3>
					<?php endif; ?>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
					<?php if (!empty($introduction['fld_intro_body'])): ?>
						<?php echo $introduction['fld_intro_body']; ?>
					<?php endif; ?>

					<?php if (!empty($introduction['fld_intro_linktopage'])): ?>
						<?php if (!empty($introduction['fld_intro_linktopage']['target'])): ?>
							<a class="btn btn-default btn-theme btn-theme--primary" href="<?php echo $introduction['fld_intro_linktopage']['url']; ?>" target="<?php echo $introduction['fld_intro_linktopage']['target']; ?>"><?php echo $introduction['fld_intro_linktopage']['title']; ?></a>
						<?php else: ?>
							<a class="btn btn-default btn-theme btn-theme--primary" href="<?php echo $introduction['fld_intro_linktopage']['url']; ?>"><?php echo $introduction['fld_intro_linktopage']['title']; ?></a>
						<?php endif; ?>
					<?php endif; ?>

				</div>
			</div>
		<?php endif; ?>

		<!-- KEY PAGES -->
		<?php $key_pages = get_field('grp_homepage_services');?>
		<?php if (!empty($key_pages['fld_services_title'])): ?>
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<h3 class="heading-h6 color-secondary heading-keyline"><?php echo $key_pages['fld_services_title']; ?></h3>
					<?php if (!empty($key_pages['fld_services_excerpt'])): ?>
						<p><?php echo $key_pages['fld_services_excerpt']; ?></p>
					<?php endif; ?>
				</div>
			</div>
			<?php if (!empty($key_pages['rpt_services_page_links'])): ?>
				<div class="row pb-md">
					<?php foreach ($key_pages['rpt_services_page_links'] as $key_page): ?>
						<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
							<?php if ($key_page['fld_service_image']): ?>
								<div class="call-overlay color-white" style="background-image:url(<?php echo $key_page['fld_service_image']['url']; ?>);">
							<?php else: ?>	
								<div class="call-overlay color-white" style="background-color:#E00051;">
							<?php endif; ?>		
								<?php if ($key_page['fld_service_linktopage']): ?>
									<a class="call-overlay--link" href="<?php echo $key_page['fld_service_linktopage']['url']; ?>"></a>
								<?php endif; ?>
								<div class="call-overlay--inner">
									<?php if(!empty($key_page['fld_service_title'])): ?>
										<h3 class="heading-h4"><?php echo $key_page['fld_service_title']; ?></h3>
									<?php endif; ?>
									<?php if(!empty($key_page['fld_service_excerpt'])): ?>
										<p class="call-overlay--reveal"><?php echo $key_page['fld_service_excerpt']; ?></p>
									<?php endif; ?>
								</div>
								<div class="call-overlay--opacity"></div>
							</div>
						</div>
					<?php endforeach; ?>
				</div>
			<?php endif; ?>
		</div>
	</div>
	<?php endif; ?>

	<!-- ABOUT PROVEK -->
	<?php $about = get_field('grp_homepage_about');?>
	<div class="section section--triangles section--triangles--auto">
		<div class="triangles top-right sml primary zindex3"></div>
		<div class="triangles top-right lrg gray zindex1"></div>

		<div class="container">
			<?php if (!empty($about['fld_about_title'])): ?>
			<div class="row pt-lg">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<h3 class="heading-h6 color-secondary heading-keyline"><?php echo $about['fld_about_title']; ?></h3>
				</div>
			</div>
			<div class="row pb-md">
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
					<?php if (!empty($about['fld_about_excerpt'])): ?>
						<h3 class="heading-h2 color-secondary"><?php echo $about['fld_about_excerpt']; ?></h3>
					<?php endif; ?>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
					<?php if (!empty($about['fld_about_body'])): ?>
						<?php echo $about['fld_about_body']; ?>
					<?php endif; ?>

					<?php if (!empty($about['fld_about_linktopage'])): ?>
						<?php if (!empty($about['fld_about_linktopage']['target'])): ?>
							<a class="btn btn-default btn-theme btn-theme--primary" href="<?php echo $about['fld_about_linktopage']['url']; ?>" target="<?php echo $about['fld_about_linktopage']['target']; ?>"><?php echo $about['fld_about_linktopage']['title']; ?></a>
						<?php else: ?>
							<a class="btn btn-default btn-theme btn-theme--primary" href="<?php echo $about['fld_about_linktopage']['url']; ?>"><?php echo $about['fld_about_linktopage']['title']; ?></a>
						<?php endif; ?>
					<?php endif; ?>
				</div>
			</div>
			<?php endif; ?>

			<!-- TESTIMONIALS/CASE STUDIES -->
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
					<?php $testimonials = new WP_Query(array('post_type' => 'testimonial', 'posts_per_page' => -1, 'orderby' => 'rand')); ?>
					<?php if ($testimonials->have_posts()) : ?>
						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<h3 class="heading-h6 color-secondary heading-keyline">Testimonials</h3>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<?php $first = true; ?>
								<div id="testimonialCarousel" class="carousel carousel-block slide carousel-block--fade carousel-block--generic" data-ride="carousel">
									<!-- CONTROLS -->
									<a class="btn btn-default carousel-block__btn carousel-block__btn-left color-primary" href="#testimonialCarousel" data-slide="prev">Previous</a>
									<a class="btn btn-default carousel-block__btn carousel-block__btn-right color-primary" href="#testimonialCarousel" data-slide="next">Next</a>
									<div class="carousel-inner carousel-block__inner">
										<?php while ($testimonials->have_posts()) : $testimonials->the_post(); ?>
											<?php 
												$testimonial = get_field('fld_testimonial_body');
												$client = get_field('fld_testimonial_client');
												$position = get_field('fld_testimonial_client_position');
												$date = get_field('fld_testimonial_date');
											?>
											<div class="item<?php echo $first ? ' active' : ''; ?> carousel-block__item">
												<!-- CAROUSEL CONTENT -->
												<?php if (!empty($testimonial)) { ?>
													<p class="heading-h5 color-secondary"><em><?php echo $testimonial; ?></em></p>
												<?php } ?>
												<?php if (!empty($position)) { ?>
													<p class="color-secondary"><?php echo $client; ?>, <?php echo $position; ?>, <?php echo $date; ?></p>
												<?php } else { ?>
													<p class="color-secondary"><?php echo $client; ?>, <?php echo $date; ?></p>
												<?php } ?>
												<!-- CAROUSEL CONTENT END -->
											</div>
										<?php $first = false; ?>
										<?php endwhile; wp_reset_query(); ?>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<a class="btn btn-default btn-theme btn-theme--primary" href="/testimonials/">View all testimonials</a>
							</div>
						</div>
					<?php endif; ?>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">

					<?php $casestudies = new WP_Query(array('post_type' => 'case_studies', 'posts_per_page' => -1, 'orderby' => 'menu_order', 'order' => 'ASC')); ?>
					<?php if ($casestudies->have_posts()) : ?>
						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<h3 class="heading-h6 color-secondary heading-keyline">Case studies</h3>
							</div>
						</div>
						<div class="row">
							<div id="successCarousel" class="carousel carousel-block slide carousel-block--generic" data-ride="carousel">
								<!-- CONTROLS -->
								<a class="btn btn-default carousel-block__btn carousel-block__btn-left color-primary" href="#successCarousel" data-slide="prev">Previous</a>
								<a class="btn btn-default carousel-block__btn carousel-block__btn-right color-primary" href="#successCarousel" data-slide="next">Next</a>
								<div class="carousel-inner carousel-block__inner">
									<?php 
										$rowCount = $casestudies->post_count;
										$first = true;
										$pageCounter = 0;
										$rowCounter = 0;
									?>

									<?php while ($casestudies->have_posts()) : $casestudies->the_post(); 
										$pageCounter++;
										$rowCounter++;
										$rightcolumn = get_field('grp_content_right', $post->ID);
									?>

									<?php if ($pageCounter==1): ?>
										<div class="item<?php echo $first ? ' active' : ''; ?>">
									<?php endif; ?>
											<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
												<?php if (!empty($rightcolumn['fld_content_right_image'])): ?>
													<a href="<?php the_permalink(); ?>" class="thumbnail-theme thumbnail-theme--logo"><img src="<?php echo $rightcolumn['fld_content_right_image']['url']; ?>" alt="<?php echo $rightcolumn['fld_content_right_image']['alt'] ?>" class="img-responsive" /></a>
												<?php endif; ?>

												<p><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></p>
												<p><a href="<?php the_permalink(); ?>" class="link-theme link-theme--primary">Read case study</a></p>
											</div>
									<?php if ($pageCounter==2 || $rowCounter==$rowCount): ?>
										</div>
									<?php endif; ?>


									<?php $first = false; ?>
									<?php if ($pageCounter==2) { $pageCounter = 0;} ?>
									
									<?php endwhile; wp_reset_query(); ?>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<a class="btn btn-default btn-theme btn-theme--primary" href="/case-studies/">View all case studies</a>
							</div>
						</div>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>

	<!-- CLIENTS -->
	<?php get_template_part('template-parts/carousels/content', 'client'); ?>

	<!-- BANNER -->
	<?php $banner = get_field('grp_homepage_banner'); ?>
	<?php if (!empty($banner['fld_banner_title'])): ?>
		<?php if (!empty($banner['fld_banner_image'])): ?>
			<div class="section section--overlay section--overlay--graphic" style="background-image:url(<?php echo $banner['fld_banner_image']['url']; ?>);background-size: cover;
    background-repeat: no-repeat;">
		<?php else: ?>
			<div class="section" style="background-color:#E00051;">
		<?php endif; ?>
				<div class="container">
					<div class="row pt-lg pb-lg">
						<div class="col-xs-12 col-sm-8 col-sm-offset-4 col-md-8 col-md-offset-4 col-lg-8 col-lg-offset-4">
							<?php if (!empty($banner['fld_banner_prefix'])): ?>
								<h3 class="heading-h6 color-white heading-keyline"><?php echo $banner['fld_banner_prefix']; ?></h3>
							<?php endif; ?>
							<h3 class="heading-h1 color-white"><?php echo $banner['fld_banner_title']; ?></h3>
							<?php if (!empty($banner['fld_banner_excerpt'])): ?>
								<p class="color-white"><?php echo $banner['fld_banner_excerpt']; ?></p>
							<?php endif; ?>

							<?php if (!empty($banner['fld_banner_linktopage'])): ?>
								<?php if (!empty($banner['fld_banner_linktopage']['target'])): ?>
									<a class="btn btn-default btn-theme btn-theme--primary" href="<?php echo $banner['fld_banner_linktopage']['url']; ?>" target="<?php echo $banner['fld_banner_linktopage']['target']; ?>"><?php echo $banner['fld_banner_linktopage']['title']; ?></a>
								<?php else: ?>
									<a class="btn btn-default btn-theme btn-theme--primary" href="<?php echo $banner['fld_banner_linktopage']['url']; ?>"><?php echo $banner['fld_banner_linktopage']['title']; ?></a>
								<?php endif; ?>
							<?php endif; ?>
							
						</div>
					</div>
				</div>
				<div class="overlay--opacity"></div>
			</div>
	<?php endif; ?>

	<!-- NEWSLETTER/NEWS/PM -->
	<?php 
		$pmchannel = get_field('grp_homepage_promo'); 
		$newsletter = get_field('fld_newsletter_excerpt');
	?>
	<div class="section section--triangles section--triangles--auto">
		<div class="triangles base-right med gray zindex1"></div>
		<div class="container">
			<div class="row pt-md pb-md">
				<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
					<h3 class="heading-h6 color-secondary heading-keyline">Newsletter Signup</h3>
					<?php if (!empty($newsletter)) { ?>
						<p><?php echo $newsletter; ?></p>
					<?php } ?>
					<!-- Begin MailChimp Signup Form -->
					<link href="//cdn-images.mailchimp.com/embedcode/classic-10_7.css" rel="stylesheet" type="text/css">
					<div class="form-block form-block--newsletter">
						<form action="https://provek.us17.list-manage.com/subscribe/post?u=38a4b9030a46e1a275f73017d&amp;id=d0eb1ad44e" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
							<div class="mc-field-group form-block--newsletter-group">
								<label for="mce-EMAIL"><input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL" placeholder="Email Address*"></label>
							</div>
							<div class="mc-field-group form-block--newsletter-group">
								<label for="mce-FNAME"><input type="text" value="" name="FNAME" class="" id="mce-FNAME" placeholder="Full Name"></label>
							</div>
							<div id="mce-responses" class="clear">
								<div class="response" id="mce-error-response" style="display:none"></div>
								<div class="response" id="mce-success-response" style="display:none"></div>
							</div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
							<div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_38a4b9030a46e1a275f73017d_d0eb1ad44e" tabindex="-1" value=""></div>
							<div class="clear"><input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
						</form>
					</div>
					<script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script>
					<noscript>This website requires Javascript. Please enable this to view the site at its best.</noscript>
					<script type='text/javascript'>(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';fnames[1]='FNAME';ftypes[1]='text';}(jQuery));var $mcj = jQuery.noConflict(true);</script>
					<noscript>This website requires Javascript. Please enable this to view the site at its best.</noscript>
					<!--End mc_embed_signup-->
				</div>
				<?php if (!empty($pmchannel['fld_promo_excerpt'])): ?>
					<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
				<?php else: ?>
					<div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
				<?php endif; ?>
					<?php $blogarticles = new WP_Query(array('post_type' => 'post', 'post_status' => 'publish', 'posts_per_page' => 2, 'orderby' => 'post_date', 'order' => 'DESC')); ?>
					<?php if ($blogarticles->have_posts()) : ?>
						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<h3 class="heading-h6 color-secondary heading-keyline">Latest News</h3>
							</div>
						</div>
						<div class="row">

							<?php while ($blogarticles->have_posts()) : $blogarticles->the_post(); ?>
							<?php 
								$leftcolumn = get_field('grp_content_left', $post->ID);
							?>
								<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
									<p class="heading-h7 color-secondary"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></p>
									<?php if (!empty($leftcolumn['fld_content_left_excerpt'])): ?>
										<p><?php echo $leftcolumn['fld_content_left_excerpt']; ?></p>
									<?php endif; ?>
									<p><a href="<?php the_permalink(); ?>" class="link-theme link-theme--primary">Read article</a></p>
								</div>
							<?php endwhile; wp_reset_query(); ?>
						</div>
						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<a class="btn btn-default btn-theme btn-theme--primary" href="/news/">View all news</a>
							</div>
						</div>
					<?php endif; ?>
				</div>
				<?php if (!empty($pmchannel['fld_promo_excerpt'])): ?>
					<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
						<h3 class="heading-h6 color-secondary heading-keyline">The PM Channel</h3>
						<div class="thumbnail-theme--pm">
							<?php if (!empty($pmchannel['fld_promo_image'])): ?>
								<div class="thumbnail-theme">
									<?php if (!empty($pmchannel['fld_promo_url'])): ?>
										<a href="<?php echo $pmchannel['fld_promo_url']; ?>" target="_blank"><img src="<?php echo $pmchannel['fld_promo_image']['url']; ?>" alt="<?php echo $pmchannel['fld_promo_image']['alt'] ?>" class="img-responsive" /></a>
									<?php else: ?>
									<img src="<?php echo $pmchannel['fld_promo_image']['url']; ?>" alt="<?php echo $pmchannel['fld_promo_image']['alt'] ?>" class="img-responsive" />
									<?php endif; ?>
								</div>
							<?php endif; ?>
							<p><?php echo $pmchannel['fld_promo_excerpt']; ?></p>
							<?php if (!empty($pmchannel['fld_promo_url'])): ?>
								<a class="btn-theme color-primary" href="<?php echo $pmchannel['fld_promo_url']; ?>" target="_blank">Find out more</a>
							<?php endif; ?>
						</div>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</div>

<?php get_footer(); ?>