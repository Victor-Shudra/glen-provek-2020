<?php get_header(); ?>

	<!-- BREADCRUMB -->
	<?php get_template_part('template-parts/breadcrumb/content'); ?>

	<!-- DEFAULT BODY CONTENT -->
	<?php get_template_part('template-parts/body/content'); ?>

	<!-- THE PROVEK WAY -->
	<?php get_template_part('template-parts/provek-way/content'); ?>

<?php get_footer(); ?>